package com.example.testformovo.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testformovo.R
import com.example.testformovo.ui.buy.BuyActivity
import com.example.testformovo.viewmodel.ProductViewModel

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var productViewModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        initViews()

        initListeners()
    }

    private fun initViews() {
        val adapter = ProductListAdapter(this)
        adapter.onProductAmountChangedListener = object : ProductListAdapter.OnProductAmountChangedListener{
            override fun onProductAmountChanged() {
                updateFabButtonEnabled()
            }
        }
        recyclerview.adapter = adapter
        recyclerview.layoutManager = LinearLayoutManager(this)

        productViewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        //Observe for data to change
        productViewModel.allProducts.observe(this, Observer { product ->
            // Update the cached copy of the products in the adapter.
            product?.let { adapter.setProducts(it) }
        })
        updateFabButtonEnabled()
    }

    private fun updateFabButtonEnabled() {
        fab.isEnabled = (recyclerview.adapter as ProductListAdapter).getProducts().any { it.amount != 0 }
    }

    private fun initListeners() {
        fab.setOnClickListener { view ->
            val intent = Intent(this@MainActivity, BuyActivity::class.java)
            intent.putParcelableArrayListExtra(BuyActivity.PRODUCT_LIST_KEY, ArrayList((recyclerview.adapter as ProductListAdapter).getProducts()))
            startActivity(intent)
        }
    }
}
