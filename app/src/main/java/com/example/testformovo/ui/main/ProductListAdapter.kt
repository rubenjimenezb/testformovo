package com.example.testformovo.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.testformovo.R
import com.example.testformovo.db.entity.Product
import com.example.testformovo.utils.imageloader.ImageLoaderImpl
import com.example.testformovo.utils.product.ProductUtils
import nl.dionsegijn.steppertouch.OnStepCallback
import nl.dionsegijn.steppertouch.StepperTouch

class ProductListAdapter internal constructor(
    private val context: Context
) : RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var products = emptyList<Product>() // Cached copy of products
    var onProductAmountChangedListener: OnProductAmountChangedListener? = null

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName: TextView = itemView.findViewById(R.id.tvName)
        val tvPrize: TextView = itemView.findViewById(R.id.tvPrize)
        val tvDiscount: TextView = itemView.findViewById(R.id.tvDiscount)
        val ivProduct: ImageView = itemView.findViewById(R.id.ivProduct)
        val stepperTouch: StepperTouch = itemView.findViewById(R.id.stepperTouch)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return ProductViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        products[position].let { product ->
            holder.tvName.text = product.name
            holder.tvPrize.text =  context.getString(R.string.price, product.price)
            ImageLoaderImpl().load(holder.ivProduct, product.getUrlProduct())
            holder.tvDiscount.text = ProductUtils.getStringForDiscount(context, product.code)

            //Stepper touch
            holder.stepperTouch.minValue = 0
            holder.stepperTouch.maxValue = MAX_PRODUCTS_VALUE
            holder.stepperTouch.sideTapEnabled = true
            holder.stepperTouch.addStepCallback(object : OnStepCallback {
                override fun onStep(value: Int, positive: Boolean) {
                    product.amount = value
                    onProductAmountChangedListener?.onProductAmountChanged()
                }
            })
        }
    }

    internal fun setProducts(products: List<Product>) {
        this.products = products
        notifyDataSetChanged()
    }

    fun getProducts() : List<Product>{
        return this.products
    }

    override fun getItemCount() = products.size

    companion object{
        const val MAX_PRODUCTS_VALUE = 100
    }

    interface OnProductAmountChangedListener{
        fun onProductAmountChanged()
    }
}