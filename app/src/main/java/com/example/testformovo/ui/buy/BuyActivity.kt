package com.example.testformovo.ui.buy

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.testformovo.R
import com.example.testformovo.db.entity.Product
import com.example.testformovo.utils.product.ProductUtils
import kotlinx.android.synthetic.main.activity_buy.*
import java.util.ArrayList


class BuyActivity : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy)
        val productList = this.intent.getParcelableArrayListExtra<Product>(PRODUCT_LIST_KEY)

        fillProductPrizes(productList)
    }

    /**
     * This function fills all the data about prices in buy activity
     */
    private fun fillProductPrizes(productList: ArrayList<Product>?) {
        var totalAmount = 0.0
        var totalDescription = ""
        productList?.filter { it.amount > 0 }?.forEach { product ->
            totalAmount += ProductUtils.getProductPriceWithDiscount(product)
            totalDescription += ProductUtils.getProductPriceDescription(this, product)
        }

        tvTotalPrizeTitle.text = getString(R.string.total_price, totalAmount)
        tvTotalPrizeDescription.text = totalDescription
    }

    companion object {
        const val PRODUCT_LIST_KEY = "PRODUCT_LIST_KEY"
    }
}
