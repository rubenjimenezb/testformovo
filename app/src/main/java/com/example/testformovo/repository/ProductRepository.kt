package com.example.testformovo.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.testformovo.db.dao.ProductDao
import com.example.testformovo.db.entity.Product
import com.example.testformovo.network.ProductService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import retrofit2.Response

class ProductRepository(
    private val productDao: ProductDao,
    private val scope: CoroutineScope
) {

    // The suspend modifier tells the compiler that this must be called from a
    // coroutine or another suspend function.
    suspend fun insert(product: Product) {
        productDao.insert(product)
    }

    fun getProducts(context: Context): LiveData<List<Product>> {

        ProductService().getProducts(context, object : ProductService.CallbackResponse<List<Product>> {
            override fun onResponse(response: List<Product>) {
                scope.launch {
                    Log.d("Test repository", "service ok renew database")
                    //Renew the local products if received
                    productDao.deleteAll()
                    response.forEach { product ->
                        productDao.insert(product)
                    }
                }
            }

            override fun onFailure(t: Throwable, res: Response<*>?) {
                //If error, the one from local will be used
                Log.d("Test repository", "error in service so use the database one: " + t.stackTrace)
            }
        })

        return productDao.getAllProducts()
    }
}