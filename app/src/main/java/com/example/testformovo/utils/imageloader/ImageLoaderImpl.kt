package com.example.testformovo.utils.imageloader

import android.widget.ImageView
import com.squareup.picasso.Picasso

class ImageLoaderImpl : ImageLoader {

    /**
     * Load image from resource
     */
    override fun load(i: ImageView, res: Int) {
        Picasso.get()
            .load(res)
            .into(i)
    }

    /**
     * Load image from url (internet)
     */
    override fun load(i: ImageView, url: String) {
        Picasso.get()
            .load(url)
            .into(i)
    }
}