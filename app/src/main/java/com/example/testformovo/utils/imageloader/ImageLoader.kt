package com.example.testformovo.utils.imageloader

import android.widget.ImageView

interface ImageLoader {
    fun load(i: ImageView, res: Int)
    fun load(i: ImageView, url: String)
}