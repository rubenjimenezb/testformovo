package com.example.testformovo.utils.product

import android.content.Context
import com.example.testformovo.R
import com.example.testformovo.db.entity.Discount
import com.example.testformovo.db.entity.Product
import kotlin.math.floor

object ProductUtils{
    private val productsWithDiscount = listOf(
        Discount(Product.VOUCHER_KEY, Discount.TWO_PER_ONE_KEY),
        Discount(Product.TSHIRT_KEY, Discount.BULK_DISCOUNT_KEY, 3, 19.0))

    /**
     * This function returns the discount for a code received
     */
    private fun returnDiscountObject(code: String) : Discount?{
        //First or null because discounts are not cumulative
        return productsWithDiscount.firstOrNull { it.code == code }
    }

    /**
     * This function returns the discount text for discount label
     */
    fun getStringForDiscount(context: Context, code: String): String{
        val discount = returnDiscountObject(code)
        return when(discount?.type){
            Discount.TWO_PER_ONE_KEY -> context.getString(R.string.discount_2_x_1)
            Discount.BULK_DISCOUNT_KEY -> context.getString(R.string.discount_bulk, discount.countUnitsForDiscount, discount.newPrizeWithDiscount)
            else -> ""
        }
    }

    /**
     * This function returns the price with the right discount
     */
    fun getProductPriceWithDiscount(product: Product): Double{
        val discount = returnDiscountObject(product.code)
        return when(discount?.type){
            Discount.TWO_PER_ONE_KEY -> calculateTwoPerOnePriceWithDiscount(product)
            Discount.BULK_DISCOUNT_KEY -> calculateBulkPriceWithDiscount(product, discount)
            else -> calculatePriceWithoutDiscount(product)
        }
    }

    /**
     * This function returns the description for the price with the right discount
     */
    fun getProductPriceDescription(context: Context, product: Product): String{
        val discount = returnDiscountObject(product.code)
        val price = getProductPriceWithDiscount(product)
        var priceDescription = context.getString(R.string.partial_price, product.amount, product.name, price)

        priceDescription += when(discount?.type){
            Discount.TWO_PER_ONE_KEY -> calculateTwoPerOnePriceDescription(context, product)
            Discount.BULK_DISCOUNT_KEY -> calculateBulkPriceDescription(context, product, discount)
            else -> calculatePriceDescriptionWithoutDiscount(context, product)
        }

        return priceDescription
    }

    /**
     * This function return the price description for a product calculated with bulk discount
     */
    private fun calculateBulkPriceDescription(context: Context, product: Product, discount: Discount): String {
        return if(product.amount >= discount.countUnitsForDiscount) {
            context.getString(R.string.partial_price_bulk, discount.newPrizeWithDiscount, discount.countUnitsForDiscount)
        }else {
            calculatePriceDescriptionWithoutDiscount(context, product)
        }
    }

    /**
     * This function return the price description for a product without discount
     */
    private fun calculatePriceDescriptionWithoutDiscount(context: Context, product: Product): String {
        return if(product.amount == 1){
            //If only one item, the description is redundant
            context.getString(R.string.partial_price_no_description)
        }else{
            context.getString(R.string.partial_price_without_discount, product.price)
        }
    }

    /**
     * This function return the price description for a product calculated with 2x1 discount
     */
    private fun calculateTwoPerOnePriceDescription(context: Context, product: Product): String {
        val freeItems = calculateTwoPerOneFreeItems(product)
        return if (freeItems == 0){
            //If not free items, the description is redundant
            context.getString(R.string.partial_price_no_description)
        }else{
            context.getString(R.string.partial_price_2_x_1, freeItems)
        }
    }

    /**
     * This function return the price for a product without discount
     */
    private fun calculatePriceWithoutDiscount(product: Product): Double{
        return product.amount * product.price
    }

    /**
     * This function return the price for a product calculated with bulk discount
     */
    private fun calculateBulkPriceWithDiscount(product: Product, discount: Discount): Double {
        return if(product.amount >= discount.countUnitsForDiscount) {
            product.amount * discount.newPrizeWithDiscount
        }else {
            calculatePriceWithoutDiscount(product)
        }
    }

    /**
     * This function return the price for a product calculated with 2x1 discount
     */
    private fun calculateTwoPerOnePriceWithDiscount(product: Product): Double{
        return (product.amount - calculateTwoPerOneFreeItems(product)) * product.price
    }

    /**
     * This function returns the number of free items with a 2x1 discount
     */
    private fun calculateTwoPerOneFreeItems(product: Product): Int {
        return floor(product.amount.toDouble().div(2)).toInt()
    }
}