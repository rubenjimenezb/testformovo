package com.example.testformovo.network

import retrofit2.Call
import retrofit2.http.GET

interface ProductApi {

    @GET("/bins/4bwec")
    fun getProductList(): Call<ProductResponse>
}