package com.example.testformovo.network

import android.content.Context
import com.example.testformovo.db.entity.Product
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ProductService {

    interface CallbackResponse<T> {
        fun onResponse(response: T)
        fun onFailure(t: Throwable, res: Response<*>? = null) {}
    }

    private val productApi: ProductApi

    init {
        // timeout
        val timeout: Long = 1 * 60 * 1000

        val client = OkHttpClient.Builder()
            .connectTimeout(timeout, TimeUnit.MILLISECONDS)
            .writeTimeout(timeout, TimeUnit.MILLISECONDS)
            .readTimeout(timeout, TimeUnit.MILLISECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl("https://api.myjson.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        productApi = retrofit.create(ProductApi::class.java)
    }

    fun getProducts(context: Context, cb: CallbackResponse<List<Product>>) {

        productApi.getProductList().enqueue(object : Callback<ProductResponse> {

            override fun onFailure(call: Call<ProductResponse>, t: Throwable) { cb.onFailure(t) }

            override fun onResponse(call: Call<ProductResponse>, response: Response<ProductResponse>) {
                if (response.isSuccessful && response.body() != null && !response.body()!!.products.isNullOrEmpty()) {
                    cb.onResponse(response.body()!!.products)
                } else {
                    cb.onFailure(Throwable(response.message()), response)
                }
            }
        })
    }
}