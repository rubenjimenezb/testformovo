package com.example.testformovo.network

import com.example.testformovo.db.entity.Product

data class ProductResponse(val products: List<Product>)