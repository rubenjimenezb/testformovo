package com.example.testformovo.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.testformovo.db.AppRoomDataBase
import com.example.testformovo.db.entity.Product
import com.example.testformovo.repository.ProductRepository
import kotlinx.coroutines.launch

class ProductViewModel(application: Application) : AndroidViewModel(application) {

    // The ViewModel maintains a reference to the repository to get data.
    private val repository: ProductRepository
    // LiveData gives us updated products when they change.
    val allProducts: LiveData<List<Product>>

    init {
        // Gets reference to ProductDao from AppRoomDataBase to construct
        // the correct ProductRepository.
        val productsDao = AppRoomDataBase.getDatabase(application, viewModelScope).productDao()
        repository = ProductRepository(productsDao, viewModelScope)
        allProducts = repository.getProducts(application.applicationContext)
    }

    // The implementation of insert() is completely hidden from the UI.
    // We don't want insert to block the main thread, so we're launching a new
    // coroutine. ViewModels have a coroutine scope based on their lifecycle called
    // viewModelScope which we can use here.
    fun insert(product: Product) = viewModelScope.launch {
        repository.insert(product)
    }
}