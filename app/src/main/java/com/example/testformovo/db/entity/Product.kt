package com.example.testformovo.db.entity

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product_table")
class Product(
    @PrimaryKey
    val name: String,
    val code: String,
    val price: Double,
    var amount : Int = 0) : Parcelable{

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readDouble(),
        parcel.readInt()
    )

    fun getUrlProduct() : String {
        return when(code){
            MUG_KEY -> MUG_URL
            TSHIRT_KEY -> TSHIRT_URL
            else -> VOUCHER_URL
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(code)
        parcel.writeDouble(price)
        parcel.writeInt(amount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Product> {
        override fun createFromParcel(parcel: Parcel): Product {
            return Product(parcel)
        }

        override fun newArray(size: Int): Array<Product?> {
            return arrayOfNulls(size)
        }

        const val VOUCHER_KEY = "VOUCHER"
        const val TSHIRT_KEY = "TSHIRT"
        const val MUG_KEY = "MUG"

        const val VOUCHER_URL = "https://cabify.com/user/themes/cabify/img/cover.png"
        const val TSHIRT_URL = "https://cabify.com/user/pages/smartcollection/_photo3/prenda-09.jpg"
        const val MUG_URL = "https://cdn.shopify.com/s/files/1/0312/6537/products/27514-Black-White-1_aef806de-0299-4603-9305-bcc83155db8f_1024x1024.jpg?v=1495633232"
    }
}