package com.example.testformovo.db.entity

class Discount(
    val code : String,
    val type : String,
    val countUnitsForDiscount : Int = 0,
    val newPrizeWithDiscount : Double = 0.0) {

    companion object{
        const val TWO_PER_ONE_KEY = "2x1"
        const val BULK_DISCOUNT_KEY = "BULK"
        const val NO_DISCOUNT_KEY = "NONE"
    }
}