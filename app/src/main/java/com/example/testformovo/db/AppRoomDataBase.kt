package com.example.testformovo.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.testformovo.db.dao.ProductDao
import com.example.testformovo.db.entity.Product
import kotlinx.coroutines.CoroutineScope

@Database(entities = arrayOf(Product::class), version = 1)
abstract class AppRoomDataBase : RoomDatabase() {

    abstract fun productDao(): ProductDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: AppRoomDataBase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): AppRoomDataBase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppRoomDataBase::class.java,
                    "product_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}