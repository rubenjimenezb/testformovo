I have done the project starting with the following google code lab https://codelabs.developers.google.com/codelabs/android-room-with-a-view-kotlin/#11
 <br/>
I used the base code from that code lab with MVVM and database with room.
 <br/>
After that I changed all the logic to adapt it to the test specifications.
 <br/>
I added also logic in repository to get the data from internet instead of the database, And save it to work offline in next failure services.